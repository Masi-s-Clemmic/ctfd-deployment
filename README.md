# CTFd deployment

Step 1 - Clone CTFd repository
```bash
git clone https://github.com/CTFd/CTFd.git
```

Step 2 - Add docker-compose.yml file to CTFd directory
```bash
mv docker-compose.yml CTFd/
```

Step 3 - Create .env file in CTFd directory
```bash
mv env.sample CTFd/.env
```

Step 4 - Go to CTFd directory
```bash
cd CTFd
```

Step 5 - Generate secret key for CTFd
```bash
head -c 64 /dev/urandom > .ctfd_secret_key
```

Step 6 - Edit .env parameters and replace value by your value

Step 7 - Generate self-signed certificate and private key
```bash
openssl req -newkey rsa:4096 -keyout <FQDN_or_IP>.key -nodes -x509 -days 365 -out <FQDN_or_IP>.crt
```

Step 8 - Create directory for certificate
```bash
sudo mkdir -p /srv/CTFd/reverse-proxy/certs
```

Step 9 - Move self-signed certificate and private key to certificate directory
```bash
sudo mv <FQDN_or_IP>.key <FQDN_or_IP>.crt /srv/CTFd/reverse-proxy/certs/
```

Step 10 - Run docker-compose
```bash
docker-compose up -d
```
